#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

class Point2D {
public:
	Point2D() {
		x = 0;
		y = 0;
	}
	Point2D(double _x, double _y) {
		x = _x;
		y = _y;
	}
	double x = 0;
	double y = 0;
};

class Vector2D {
public:
	Vector2D() {
		x = 0;
		y = 0;
	}
	Vector2D(Point2D _begin, Point2D _end) {
		x = _end.x - _begin.x;
		y = _end.y - _begin.y;
	}
	Vector2D(double _x, double _y) {
		x = _x;
		y = _y;
	}
	double x;
	double y;
};

Point2D operator-(const Point2D& p) {
	Point2D res(-p.x, -p.y);
	return res;
}

Point2D operator+(const Point2D& p1, const Point2D& p2) {
	return Point2D(p1.x + p2.x, p1.y + p2.y);
}

void operator+=(Vector2D& first_vector, const Vector2D& second_vector) {
	first_vector.x += second_vector.x;
	first_vector.y += second_vector.y;
}
Vector2D operator+(const Vector2D& first_vector, const Vector2D& second_vector) {
	Vector2D result;
	result += first_vector;
	result += second_vector;
	return result;
}

void operator-=(Vector2D& first_vector, const Vector2D& second_vector) {
	first_vector.x -= second_vector.x;
	first_vector.y -= second_vector.y;
}

Vector2D operator-(const Vector2D& first_vector, const Vector2D& second_vector) {
	Vector2D result;
	result += first_vector;
	result -= second_vector;
	return result;
}

Vector2D operator*(const Vector2D& vector, double scal) {
	Vector2D result;
	result.x = vector.x * scal;
	result.y = vector.y * scal;
	return result;
}


double operator*(const Vector2D& first_vector, const Vector2D& second_vector) {
	return first_vector.x * second_vector.x + first_vector.y * second_vector.y;
}

Point2D read_point() {
	Point2D result;
	cin >> result.x >> result.y;
	return result;
}

bool cmp_angle(Vector2D a, Vector2D b) {
	return (a.x * b.y - a.y * b.x <= 0);
}


int main() {
	Point2D z(0, 0);
	int first_size = 0, second_size = 0;
	cin >> first_size;
	vector<Point2D> first_poligon(first_size);
	vector<Point2D> new_first_poligon(first_size);
	for (int i = 0; i < first_size; i++) {
		first_poligon[i] = read_point();
	}

	int min = 0;
	for (int i = 0; i < first_poligon.size(); i++) {
		if ((first_poligon[i].y < first_poligon[min].y) ||
			(first_poligon[i].y == first_poligon[min].y) && 
			(first_poligon[i].x < first_poligon[min].x)) {
			min = i;
		}
	}
	for (int i = 0; i < first_poligon.size(); i++) {
		new_first_poligon[i] = first_poligon[(min + i) % first_size];
	}
	new_first_poligon.push_back(new_first_poligon[0]);
	cin >> second_size;
	vector<Point2D> second_poligon(second_size);
	vector<Point2D> new_second_poligon(second_size);
	for (int j = 0; j < second_size; j++) {
		second_poligon[j] = -read_point();
	}

	min = 0;
	for (int i = 0; i < second_poligon.size(); i++) {
		if ((second_poligon[i].y < second_poligon[min].y) ||
			(second_poligon[i].y == second_poligon[min].y) && 
			(second_poligon[i].x < second_poligon[min].x)) {
			min = i;
		}
	}
	for (int i = 0; i < second_poligon.size(); i++) {
		new_second_poligon[i] = second_poligon[(min + i) % second_size];
	}
	new_second_poligon.push_back(new_second_poligon[0]);
	vector<Point2D> result;
	int i = 1;
	int j = 1;
	while (i <= first_size && j <= second_size) {
		Vector2D a(new_first_poligon[i - 1], new_first_poligon[i]);
		Vector2D b(new_second_poligon[j - 1], new_second_poligon[j]);
		result.push_back(new_first_poligon[i - 1] + new_second_poligon[j - 1]);
		if (cmp_angle(a, b)) {
			i++;
		}
		else {
			j++;
		}
	}
	while (i <= first_size) {
		result.push_back(new_first_poligon[i - 1] + new_second_poligon[j - 1]);
		i++;
	}
	while (j <= second_size) {
		result.push_back(new_first_poligon[i - 1] + new_second_poligon[j - 1]);
		j++;
	}
	for (int i = 1; i < result.size(); i++) {
		Vector2D radius_vector(z, result[i]);
		Vector2D side(result[i - 1], result[i]);
		if (!cmp_angle(radius_vector, side)) {
			cout << "NO" << endl;
			return 0;
		}
	}
	Vector2D radius_vector(z, result[result.size() - 1]);
	Vector2D side(result[result.size() - 1], result[0]);
	if (!cmp_angle(radius_vector, side)) {
		cout << "NO" << endl;
		return 0;
	}
	cout << "YES" << endl;
	return 0;
}
